package com.testcontainers.testcontainers;

import com.testcontainers.testcontainers.post.Post;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.containers.wait.strategy.WaitAllStrategy;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
public class PostControllerTest {
    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:alpine").
           withExposedPorts(5432).
            waitingFor(
                    new WaitAllStrategy()
                            .withStrategy(Wait.forLogMessage(".*database system is ready to accept connections.*\\s", 2))
                            .withStrategy(Wait.forListeningPort())
            );

//    @Container
//    @ServiceConnection
//    static MySQLContainer<?> mysql = new MySQLContainer<>("mysql:latest");
//

    @Autowired
    TestRestTemplate restTemplate;

    @BeforeAll
    static void beforeAll() throws IOException, InterruptedException {
        //This shouldn't be here...i guess there is a bug
        List<ProcessBuilder> builders = Arrays.asList(
                new ProcessBuilder("docker-compose", "up"));

        List<Process> processes = ProcessBuilder.startPipeline(builders);
        for (Process it: processes){
            it.waitFor(10000, TimeUnit.MILLISECONDS);
        }
    }

    @AfterAll
    static void afterAll() throws IOException, InterruptedException {
        //This shouldn't be here...i guess there is a bug
        List<ProcessBuilder> builders = Arrays.asList(
                new ProcessBuilder("docker-compose", "down"));

        List<Process> processes = ProcessBuilder.startPipeline(builders);
        for (Process it: processes){
            it.waitFor(10000, TimeUnit.MILLISECONDS);
        }
    }

    @Test
    public void test() throws InterruptedException {
        Post[] posts = restTemplate.getForObject("/api/posts", Post[].class);
        assertThat(posts.length).isEqualTo(6);
    }


    @Test
    public void shouldThrowNotFound() {
        ResponseEntity<Post> response = restTemplate.exchange("/api/posts/10", GET, null, Post.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isNotNull();
    }

    @Test
    public void shoouldFailInvalidPost() {
        Post p = new Post(7,2,"","",0);
        ResponseEntity<Post> response = restTemplate.exchange("/api/posts", POST, new HttpEntity<>(p), Post.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }


}