package com.testcontainers.testcontainers;


import com.testcontainers.testcontainers.post.Post;
import com.testcontainers.testcontainers.post.PostRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@Testcontainers
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PostRepositoryTest {

    @Container
    @ServiceConnection
    static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:alpine").
            withInitScript("post-schema.sql");

    @Autowired
    PostRepository postRepository;

    @Test
    void connectionEstabilished() {
        assertThat(postgres.isCreated()).isTrue();
        assertThat(postgres.isRunning()).isTrue();

    }

    @BeforeEach
    void setup() {
        List<Post> posts = List.of(
                new Post(1, 1, "Hey there", "in Bucharest is nice", 0),
                new Post(2, 2, "wassup", "Outside is cold", 0),
                new Post(3, 2, "that was nice", "body", 0)
        );
        postRepository.saveAll(posts);
    }

    @Test
    void shouldReturnPostByTitle(){
        Optional<Post> post = postRepository.findByTitle("wassup");
        assertThat(post.isPresent()).isTrue();
        assertThat(post.get().getBody()).isEqualTo("Outside is cold");
    }


}
