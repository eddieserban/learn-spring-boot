DROP table IF  EXISTS user;

CREATE TABLE IF NOT EXISTS user(
    id INT NOT NULL,
    name varchar(256) NOT NULL,
    email varchar(256),
    age INT,
    PRIMARY KEY (id)
);