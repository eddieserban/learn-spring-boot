DROP table IF  EXISTS post;

CREATE TABLE IF NOT EXISTS post(
    id SERIAL,
    user_id INT NOT NULL,
    title varchar(250) NOT NULL,
    body text NOT NULL,
    version INT,
    PRIMARY KEY (id)
);