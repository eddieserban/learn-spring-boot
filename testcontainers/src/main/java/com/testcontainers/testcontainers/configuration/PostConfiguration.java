package com.testcontainers.testcontainers.configuration;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.init.DataSourceScriptDatabaseInitializer;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.sql.init.DatabaseInitializationSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static org.springframework.boot.sql.init.DatabaseInitializationMode.ALWAYS;
import static org.springframework.boot.sql.init.DatabaseInitializationMode.EMBEDDED;

@Configuration(proxyBeanMethods = false)
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = "com.testcontainers.testcontainers.post",
        entityManagerFactoryRef = "postEntityManager",
        transactionManagerRef = "postTransactionManager"
)
public class PostConfiguration {
    //*****************POST
    @Bean
    @Primary
    @ConfigurationProperties("app.datasource.blog")
    DataSourceProperties getPostDataSource() {
        return new DataSourceProperties();
    }

    @Bean
    @Primary
    HikariDataSource postDataSource(DataSourceProperties dataSourceProperties) {
        return dataSourceProperties.initializeDataSourceBuilder().
                type(HikariDataSource.class).build();
    }

    @Bean
    DataSourceScriptDatabaseInitializer postDataSourceScriptDBInitializer(@Qualifier("postDataSource") DataSource dataSource) {
        DatabaseInitializationSettings settings = new DatabaseInitializationSettings();
        settings.setSchemaLocations(List.of("classpath:post-schema.sql"));
        settings.setMode(ALWAYS);
        return new DataSourceScriptDatabaseInitializer(dataSource, settings);
    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean postEntityManager(
            EntityManagerFactoryBuilder builder,
            @Qualifier("postDataSource") DataSource postDataSource) {
        return builder.dataSource(postDataSource)
                .packages("com.testcontainers.testcontainers.post")
                .persistenceUnit("post").build();
    }


    @Bean
    public PlatformTransactionManager postTransactionManager(
            @Qualifier("postEntityManager") LocalContainerEntityManagerFactoryBean postsEntityManagerFactory) {
        return new JpaTransactionManager(Objects.requireNonNull(postsEntityManagerFactory.getObject()));
    }

}
