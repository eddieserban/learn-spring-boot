package com.testcontainers.testcontainers.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.jdbc.init.DataSourceScriptDatabaseInitializer;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.sql.init.DatabaseInitializationSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;
import java.util.List;

import static org.springframework.boot.sql.init.DatabaseInitializationMode.EMBEDDED;

@Configuration(proxyBeanMethods = false)
public class UserConfiguration {


    //**************USERS

    @Bean
    @ConfigurationProperties("app.datasource.user")
    DataSourceProperties getUserDataSource() {
        return new DataSourceProperties();
    }


    @Bean
    DataSource userDataSource(@Qualifier("getUserDataSource") DataSourceProperties properties) {
        return DataSourceBuilder.create().
                url(properties.getUrl()).
                username(properties.getUsername()).
                password(properties.getPassword()).
                build();

    }

    @Bean
    DataSourceScriptDatabaseInitializer userDataSourceScriptDBInitializer(@Qualifier("userDataSource") DataSource dataSource) {
        DatabaseInitializationSettings settings = new DatabaseInitializationSettings();
        settings.setSchemaLocations(List.of("classpath:user-schema.sql"));
        settings.setMode(EMBEDDED);
        return new DataSourceScriptDatabaseInitializer(dataSource, settings);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean userEntityManager(EntityManagerFactoryBuilder builder,
                                                                    @Qualifier("userDataSource") DataSource userDataSource) {
        return builder.dataSource(userDataSource)
                .packages("com.testcontainers.testcontainers.user")
                .persistenceUnit("user").build();
    }


}
