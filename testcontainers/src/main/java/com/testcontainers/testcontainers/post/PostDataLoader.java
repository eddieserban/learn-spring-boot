package com.testcontainers.testcontainers.post;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.asm.TypeReference;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class PostDataLoader implements ApplicationRunner {

    private static final Logger logger = LoggerFactory.getLogger(PostDataLoader.class);
    private final PostRepository postRepository;
    private final ObjectMapper objectMapper;

    public PostDataLoader(PostRepository postRepository, ObjectMapper objectMapper) {
        this.postRepository = postRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        String POST_JSON = "/data/posts.json";
        logger.info(" Loading posts into database from JSON: {}", POST_JSON);
        if(postRepository.count()==0) {
            try(InputStream is = TypeReference.class.getResourceAsStream(POST_JSON)){
                Posts posts = objectMapper.readValue(is, Posts.class);
                postRepository.saveAll(posts.posts());
            } catch (IOException exception){
                throw new RuntimeException("Failed to read/parse json data !");
            }
        }
    }
}
