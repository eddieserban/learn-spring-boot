package com.testcontainers.testcontainers.post;

import java.util.List;

public record Posts(List<Post> posts) {
}
