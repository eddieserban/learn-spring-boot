package com.api.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@ResponseBody
public class ApiApplication {

	@GetMapping("/hello")
	String getHello() {
		return "hello";
	}


	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class, args);
	}

}
